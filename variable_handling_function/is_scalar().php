//is_scalar — Finds whether a variable is a scalar

//bool is_scalar ( mixed $var )

//    Note:

    is_scalar() does not consider NULL to be scalar.

<?php

$a="";
$b=is_scalar($a);
echo $b;  //Print 1                             //this the difference from boolval
echo "<br>";

$c=null;
$d=is_scalar($c);
echo $d;  //Print nothing
echo "<br>";
echo "<br>";
print_r($d);  //print nothing

 $e=" ";
$f=is_scalar($e);
echo $f;  //Print 1
echo "<br>";

$g=1;
$h=is_scalar($g);
echo $h; //Print 1
echo is_scalar($g); //print 1
echo "<br>";
var_dump($h); //print bool(true)
echo "<br>";  

$i=array();
$j=is_scalar($i);
echo $j; //Print nothing  
echo "<br>";
echo is_scalar($i);
echo "<br>";
var_dump($j);  //print bool(false)
echo "<br>";
print_r($j);  // print nothing

$k=false;
$m=is_scalar($k);
echo "<br>";
echo ($m); //print 1
echo "<br>";
var_dump($m);  //print bool(true)
echo "<br>";
print_r($m);  // print 1



//is_scalar function ti NULL and Array te kaj kore na baki sob 
//data type a kaj kore hok seta empty or not empty 