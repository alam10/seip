
<?php
//var_export 
$a = array (1, 2, array ("a", "b", "c"));
$b=var_export($a,true); // return string 
echo "<pre>";
echo($b); //print string 
////When the return parameter 
////is TRUE, this function will return a string. 
////Otherwise, the return nothing . 
/*array (
  0 => 1,
  1 => 2,
  2 => 
  array (
    0 => 'a',
    1 => 'b',
    2 => 'c',
  ),
)*/

?>
<?php
//var_export 
$a = array (1, 2, array ("a", "b", "c"));
$b=var_export($a,true); // return nothing  it print itself 
echo "<pre>";
echo($b); //print nothing 
////When the return parameter 
////is TRUE, this function will return a string. 
////Otherwise, the return nothing . 
/*array (
  0 => 1,
  1 => 2,
  2 => 
  array (
    0 => 'a',
    1 => 'b',
    2 => 'c',
  ),
)*/

?>

<?php
// print_r 
$a = array (1, 2, array ("a", "b", "c"));
echo "<pre>";
print_r($a);
/*Array
(
    [0] => 1
    [1] => 2
    [2] => Array
        (
            [0] => a
            [1] => b
            [2] => c
        )

)*/
?>
<?php
//var_dump 
$a = array (1, 2, array ("a", "b", "c"));
echo "<pre>";
var_dump($a);

/*array(3) {
  [0]=>
  int(1)
  [1]=>
  int(2)
  [2]=>
  array(3) {
    [0]=>
    string(1) "a"
    [1]=>
    string(1) "b"
    [2]=>
    string(1) "c"
  }
}*/
?>