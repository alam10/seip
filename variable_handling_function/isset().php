//bool isset ( mixed $var [, mixed $... ] )
<?php

$a="";
$b=isset($a);
echo $b;  //Print 1
echo "<br>";

$c=null;
$d=isset($c);
echo $d;  //Print nothing
echo "<br>";
echo "<br>";
print_r($d);  //print nothing

$e=" ";
$f=isset($e);
echo $f;  //Print 1 
echo "<br>";


$g=2.34;
$h=isset($g);
echo $h;                   //Print 1
echo isset($g);                //Print 1
echo "<br>";
var_dump($h); //Print bool(true)
echo "<br>";  

$i=array();
$j=isset($i);
echo $j; //Print 1
echo "<br>";
echo isset($i); //Print 1
echo "<br>";
var_dump($j);  //bool(true)
echo "<br>";
print_r($j);  // print 1
echo "<br>";


unset($g);
$pp=isset($g);
echo "$pp";
var_dump($pp); //print bool(false)
