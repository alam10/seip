//serialize — Generates a storable representation of a valued
//string serialize ( mixed $value )
// This is useful for storing or passing PHP values around
// without losing their type and structure.
//To make the serialized string into a PHP 
//value again, use unserialize(). 


// It is mainly used to handel session data .

<?php  
  echo "<br>";
    $a = serialize(array("arrange"=>"we","are",25,false,null));   //serialize()
    
    //Output= a:5:{s:7:"arrange";s:2:"we";i:0;s:3:"are";i:1;i:25;i:2;b:0;i:3;N;}
    
    
    echo  $a;  
    echo "<pre>";
    print_r($a);
    echo "<br>";
    echo "<pre>";
    var_dump($a);
    
    
  // unserialize — Creates a PHP value from a stored representation 
    // mixed unserialize ( string $str [, array $options ] )
//unserialize() takes a single serialized variable and converts it back into a PHP value. 
    
     $b=unserialize($a);   // unserialize ()
     echo  $b;  
    echo "<pre>";
    print_r($b);
    /* output = Array
(
    [arrange] => we
    [0] => are
    [1] => 25
    [2] => 
    [3] => 
)*/
    echo "<br>";
    echo "<pre>";
    var_dump($b);
    /* 
array(5) {
  ["arrange"]=>
  string(2) "we"
  [0]=>
  string(3) "are"
  [1]=>
  int(25)
  [2]=>
  bool(false)
  [3]=>
  NULL
}
        */
    
    ?>  
<?php  
  echo "<br>";
   $bb=5.7;    
    $a = serialize($bb);  
    echo  $a;  
    echo "<pre>";
    print_r($a);
    echo "<br>";
    echo "<pre>";
    var_dump($a);
     $b=unserialize($a);
     echo  $b;  
    echo "<pre>";
    print_r($b);
    echo "<br>";
    echo "<pre>";
    var_dump($b);
    /*
d:5.70000000000000017763568394002504646778106689453125;

d:5.70000000000000017763568394002504646778106689453125;

string(55) "d:5.70000000000000017763568394002504646778106689453125;"
5.7

5.7

float(5.7)
       */
    ?>  
/*

When serializing objects, PHP will attempt to call the 
member function __sleep() prior to serialization. This is to 
allow the object to do any last minute clean-up, etc. prior to 
being serialized. Likewise,
when the object is restored using unserialize() the __wakeup() 
member function is called. 
*/