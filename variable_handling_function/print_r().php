//mixed print_r ( mixed $expression [, bool $return = false ] )

 print_r() displays information about a variable in a way that's readable by humans.

print_r(), var_dump() and var_export() will also show protected
and private properties of objects with PHP 5. 
Static class members will not be shown. 
If you would like to capture the output of print_r(), use the return parameter.
When this parameter is set to TRUE, 
print_r() will return the information rather than print it. 



<?php
echo "<pre>";
$a = array ('a' => 'apple', 'b' => 'banana', 'c' => array ('x', 'y', 'z'));
$about=print_r ($a,false); // print_r() will  print 

echo "<pre>";
echo $about; //print the value of $about  is 1 for return 1 
////When the return parameter 
////is TRUE, this function will return a string. 
////Otherwise, the return value is TRUE. 
?>

<?php
$a = array ('a' => 'apple', 'b' => 'banana', 'c' => array ('x', 'y', 'z'));
print_r ($a);  //
echo "<pre>";
?>


<?php
$a = array ('a' => 'apple', 'b' => 'banana', 'c' => array ('x', 'y', 'z'));
$about=print_r ($a); // print_r() will  print 

echo "<pre>";
echo $about; //print the value of $about  is 1 for return 1
////When the return parameter 
////is TRUE, this function will return a string. 
////Otherwise, the return value is TRUE. 
?>

<?php
$a = array ('a' => 'apple', 'b' => 'banana', 'c' => array ('x', 'y', 'z'));
$about=print_r ($a,true); // print_r() will  not print 

echo "<pre>";
echo $about; //print the value of $about  is print_r 
?>



<?php
  $a=12;
  print_r($a); //print 12
  echo "<br>";
  $b=null;
  print_r($b); //print nothing 
  echo "<br>";
  $c=false;
  print_r($c); //print nothing for true it will 1
  echo "<br>";
  $d="we are ";
  print_r($d); //print we are 

?>