//bool is_null ( mixed $var )

<?php

$a="";
$b=is_null($a);
echo $b;  //Print nothing
echo "<br>";

$c=null;
$d=is_null($c);
echo $d;  //Print 1 for null 
echo "<br>";
echo "<br>";
print_r($d);  //print 1

$e=" ";
$f=is_null($e);
echo $f;  //Print nothing  
echo "<br>";


$g=2.34;
$h=is_null($g);
echo $h; //Print nothing 
echo is_null($g);
echo "<br>";
var_dump($h); //print bool(false)
echo "<br>";  

$i=array();
$j=is_null($i);
echo $j; //Print nothing  
echo "<br>";
echo is_null($i);
echo "<br>";
var_dump($j);  //print bool(false)
echo "<br>";
print_r($j);  // print nothing