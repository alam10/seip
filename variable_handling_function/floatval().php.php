//float floatval(mixed $val);

<?php
$a=2.0;
$b=floatval($a); //print 2
echo floatval($a); //print 2
echo "<br>";
echo $b;
echo "<br>";
var_dump(floatval($a)); //print 2 with datatype for var_dump()
echo "<br>";
var_dump($b); //print 2 with datatype for var_dump()


$c=true;
echo "<br>";
echo floatval($c);  //Print 1 for true
echo "<br>";
var_dump(floatval($c));  //print float(1)
$d=false;
echo "<br>";
echo floatval($d);  //Print 0 for false 
echo "<br>";
var_dump(floatval($d));  //print float(0)


$f="2we are";
echo "<br>";
echo floatval($f);  //Print 2
echo "<br>";
var_dump(floatval($f));  //float(2)
 
$e="we are";
echo "<br>";
echo floatval($e);  //Print 0
echo "<br>";
var_dump(floatval($e));  //float(0)

$g =null;
echo "<br>";
echo floatval($g);  //Print 0
echo "<br>";
var_dump(floatval($g));  //float(0)


$ar=array();

echo "<br>";
echo floatval($ar);  //Print 0
echo "<br>";
var_dump(floatval($ar));  //float(0)


$arr=array("we",2);  // will print 1 for any value

echo "<br>";
echo floatval($arr);  //Print 1
echo "<br>";
var_dump(floatval($arr));  //float(1)