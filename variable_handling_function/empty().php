//bool empty(mixed $val);

<?php

$a="";
$b=empty($a);
echo $b;  //Print 1 for empty string 
echo "<br>";

$c=null;
$d=empty($c);
echo $d;  //Print 1 for null 
echo "<br>";
echo "<br>";
print_r($d);  //print 1

$e=" ";
$f=empty($e);
echo $f;  //Print nothing for white space or string also 
echo "<br>";


$g=2.34;
$h=empty($g);
echo $h; //Print nothing for integer or  fraction number 
echo empty($g);
echo "<br>";
var_dump($h);
echo "<br>";  

$i=array();
$j=empty($i);
echo $j; //Print 1 for empty array but print nothing for with value array  
echo "<br>";
echo empty($i);
echo "<br>";
var_dump($j);  //bool(true)
echo "<br>";
print_r($j);  // print 1