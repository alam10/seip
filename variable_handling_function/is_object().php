//bool is_object ( mixed $var )
<?php
$a="";
$b=is_object($a);
echo $b;  //Print nothing
echo "<br>";

$c=null;
$d=is_object($c);
echo $d;  //Print nothing
echo "<br>";
echo "<br>";
print_r($d);  //print nothing

$e=" ";
$f=is_object($e);
echo $f;  //Print nothing 
echo "<br>";


$g=2.34;
$h=is_object($g);
echo $h; //Print nothing 
echo is_object($g);
echo "<br>";
var_dump($h); //Print bool(false)
echo "<br>";  

$i=array();
$j=is_object($i);
echo $j; //Print nothing
echo "<br>";
echo is_object($i);
echo "<br>";
var_dump($j);  //bool(false)
echo "<br>";
print_r($j);  // print nothing

