//var_dump — Dumps information about a variable's

//void var_dump ( mixed $expression [, mixed $... ] )

//This function displays structured information about one or more expressions 
//that includes its type and value. 
//Arrays and objects are explored recursively with values indented to show structure. 

<?php
$a = array(1, 2, array("a", "b", "c"));
var_dump($a);
/*
Outoup :
 array(3) {
  [0]=>
  int(1)
  [1]=>
  int(2)
  [2]=>
  array(3) {
    [0]=>
    string(1) "a"
    [1]=>
    string(1) "b"
    [2]=>
    string(1) "c"
  }
}
  */
?>

<?php

$b = 3.1;
$c = true;
var_dump($b, $c);
/*
Output:
 float(3.1)
bool(true)
 *  */

?>


