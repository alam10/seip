//bool is_bool ( mixed $var )

<?php

$a="";
$b=is_bool($a);
echo $b;  //Print nothing
echo "<br>";

$c=false;   //bool are true or false 
$d=is_bool($c);
echo $d;  //Print 1 for null 
echo "<br>";
echo "<br>";
print_r($d);  //print 1
var_dump($d); // print bool(true)

$e=" ";
$f=is_bool($e);
echo $f;  //Print nothing  
echo "<br>";


$g=2.34;
$h=is_bool($g);
echo $h; //Print nothing 
echo is_bool($g);
echo "<br>";
var_dump($h); //print bool(false)
echo "<br>";  

$i=array();
$j=is_bool($i);
echo $j; //Print nothing  
echo "<br>";
echo is_bool($i);
echo "<br>";
var_dump($j);  //print bool(false)
echo "<br>";
print_r($j);  // print nothing