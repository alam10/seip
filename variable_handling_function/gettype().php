//gettype — Get the type of a variable's

//string gettype ( mixed $var )

// Possible values for the returned string are:

    "boolean"
    "integer"
    "double" (for historical reasons "double" is returned in case of a float, and not simply "float")
    "string"
    "array"
    "object"
    "resource"
    "NULL"
    "unknown type"


<?php

$data = array(1, 1., NULL, new stdClass, 'foo');
echo "<br>";
foreach ($data as $value) {
    echo gettype($value);
    echo "<br>";
}
/* 
 integer
double
NULL
object
string
 */
?>




