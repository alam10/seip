<?php
// Index array 
$a=array(12,13,44,55);  //Indexing array since it has only value not key
echo $a[1];    //  print of index number of 1
echo "</br>";
echo "<pre>";
var_dump($a);  //print array with types and count 
echo "</br>";
echo "<pre>";  // Print with good organize
print_r($a);  //Print without data types and count  

//Associative array 
$b=array("asia"=>"Bangladesh","south_america"=>"usa","afrika"=>"south Afrika");
foreach($b as $cc=>$dd){
    echo $cc."=".$dd;
    echo "<br>";
}

//Mixed of index and associative arrat 
$c=array("name"=>"saidur","age"=>25,"rahman");  // Last child's index number 0 since height is null
echo "<pre>";  // Print with good organize
print_r($c);  //Print without data types and count  

$d=array("name"=>"saidur","age"=>25,5=>"rahman", "yamin"); // Last child's index number 6 since height is 5
echo "<pre>";  // Print with good organize
print_r($d);  //Print without data types and count  

// Multi dimentional Array  with indxing and associative array
$e=array("Country"=>"Bangladesh",array("Dhaka","Rajbari"),array("Joshor",12,44),5=>"Kathalbagan" ,"Dhanmondi");
echo $e["Country"];
echo "<br>";
echo $e[1][1];
echo "<br>";
echo $e[6];
echo "<br>";
echo "<pre>";  // Print with good organize
print_r($e);  //Print without data types and count  


// Replace index number with value and new index is 12 since height is 11
$f=array("ba"=>"banglasedh",11=>"dhaka",11=>"Rajbari", 4=>"Rajshahi","chi");
echo "<br>";
echo "<pre>";  // Print with good organize
print_r($f);  //Print without data types and count  

//Special replace of array
$g=  [true=>"Canada",1=>"Austruliya",1.6=>"japan",0.45=>"China"];
echo "<br>";
echo "<pre>";  // Print with good organize
print_r($g);  //Print without data types and count  

//A array must have key and value if key is null then it is called index array . 
//If have both of key and value it is called associative array 
//if we want to use any string the must use "" or '' even if you want to print index value of array whic key is string 
// Example: echo["name"];
/*We can use only string or integer as array index number if we use false it is means 0 
 for true is 1 
 
 */
